import System.Environment
import System.Directory
import System.Exit
import System.IO
import System.Path
import Debug.Trace
import Control.Monad
import Control.Concurrent
import Data.Maybe
import System.Posix.Signals
import Control.Concurrent
import qualified Control.Exception as E
import System.Console.ANSI

-- openFile :: FilePath -> IOMode -> IO Handle
-- hClose :: Handle -> IO ()
-- hFlush :: Handle -> IO ()
-- hGetChar :: Handle -> IO Char
-- hGetLine :: Handle -> IO [Char]
-- hGetContents :: Handle -> IO [Char]
-- hSeek :: Handle -> SeekMode -> Integer -> IO ()
-- hTell :: Handle -> IO Integer
-- hFileSize :: Handle -> IO Integer
-- hIsEOF :: Handle -> IO Bool
--
--
--

colors :: [Color]
colors = [Red,Green,Yellow,Blue,Magenta,Cyan]
intensities :: [ColorIntensity]
intensities = [Vivid,Dull]

setStyle :: (Color,ColorIntensity) -> IO ()
setStyle (c,i) = setSGR [SetColor Foreground i c]

resetStyle :: IO ()
resetStyle = setSGR [Reset]


styles :: [(Color,ColorIntensity)]
styles = [(c,i) | i <- intensities, c <- colors]

data FollowFile = FollowFile { handle   :: Handle
                             , filePath :: FilePath
                             , lastSize :: Integer
                             , style    :: (Color,ColorIntensity)
                             } deriving (Show,Eq)

main :: IO ()
main = do
    threadId <- myThreadId
    targets <- getArgs

    if null targets
    then (do
            putStrLn "Please provide list of files to follow."
            exit [] threadId
         )
    else (do

        ffs <- makeFollowFiles (zip targets (cycle styles))
        installHandler keyboardSignal (Catch (exit (map handle ffs) threadId)) Nothing
        run ffs
        return ()
        )


run :: [FollowFile] -> IO ()
run ffs = do
    ffs <- iter ffs
    threadDelay(1000)
    run ffs

iter :: [FollowFile] -> IO [FollowFile]
iter ffs = do
    newFFs <- sequence $ map iterSingle ffs
    return newFFs

iterSingle :: FollowFile -> IO FollowFile
iterSingle ff = do
    (h,maybeLine) <- tryReadCompletedLine (handle ff)
    if isNothing maybeLine
    then return $ FollowFile h (filePath ff) (lastSize ff) (style ff)
    else (do
        setStyle $ style ff
        putStr $ (filePath ff) ++ " :: " ++ fromJust maybeLine
        resetStyle
        hFlush stdout
        return $ FollowFile h (filePath ff) (lastSize ff) (style ff)
         )

tryReadCompletedLine :: Handle -> IO (Handle,Maybe String)
tryReadCompletedLine h = do
    currentPos <- hGetPosn h
    maybeLine <- tryGetNextCharsUntilNewLine "" h
    if isNothing maybeLine
    then (do
        hSetPosn currentPos
        return (h,Nothing))
    else return (h,maybeLine)

tryGetNextCharsUntilNewLine :: String -> Handle -> IO (Maybe String)
tryGetNextCharsUntilNewLine s h = do
    isAtEOF <- hIsEOF h
    if isAtEOF
    then return Nothing
    else (do
        c <- hGetChar h
        if c == '\n'
        then return (Just $ s ++ [c])
        else tryGetNextCharsUntilNewLine (s ++ [c]) h
         )
    
    


exit :: [Handle] -> ThreadId -> IO ()
exit handles threadId = do
    mapM_ hClose handles
    E.throwTo threadId ExitSuccess



makeAbsolute :: FilePath -> IO FilePath
makeAbsolute fp = do
    cwd <- getCurrentDirectory
    let absPath = absNormPath cwd fp
    if isJust absPath
    then return $ fromJust absPath
    else (do
        putStrLn $ "Error unable to make absolute path for target: " ++ fp ++ " (about to die)."
        let _ = fromJust absPath
        return "")



makeHandles :: [FilePath] -> IO [Handle]
makeHandles = liftM catMaybes . sequence . map makeHandle 

makeHandle :: FilePath -> IO (Maybe Handle)
makeHandle fp = do
    fileExists <- doesFileExist fp
    if not fileExists
    then (do
            putStrLn $ "Unable to find target " ++ fp
            return Nothing
         )
    else (do
            h <- openFile fp ReadMode
            return (Just h)
         )


makeFollowFiles :: [(FilePath,(Color,ColorIntensity))] -> IO [FollowFile]
makeFollowFiles = liftM catMaybes . sequence . map makeFollowFile 

makeFollowFile :: (FilePath,(Color,ColorIntensity)) -> IO (Maybe FollowFile)
makeFollowFile (fp,(c,i)) = do
    maybeHandle <- makeHandle fp
    if isNothing maybeHandle
    then return Nothing
    else (do
            let handle = fromJust maybeHandle
            hSeek handle SeekFromEnd 0
            fileSize <- hFileSize handle
            return $ Just $ (FollowFile handle fp fileSize (c,i))
         )
        
    

    
    

--main :: IO ()
--main = do
--    targets <- getArgs
--    workingDirectory <- getCurrentDirectory
--    putStrLn $ "Targets:           " ++ show targets
--    putStrLn $ "Working directory: " ++ workingDirectory
--
--    h <- openFile (head targets) ReadMode
--    run h
--
--
--    exitWith ExitSuccess
--
--run :: Handle -> IO ()
--run h = do
--    iter h
--    threadDelay 100000
--    run h
--
--iter :: Handle -> IO ()
--iter h = do
--    available <- hIsEOF h
--    s <- hFileSize h
--    putStrLn $ "size: " ++ show s
--    if available
--    then return ()
--    else (do
--        c <- hGetChar h
--        putStrLn [c]
--        return ())


